let
  pkgs = import (import ./nix/sources.nix).nixpkgs {};

  # The server configuration
  configuration = import ./configuration.nix;

  # The configuration of a VM looking like the server
  vmConfiguration = import ./vm.nix;

  # Make a system from a configuration
  makeSystem = config:
    import (pkgs.path + /nixos) {
      system = "x86_64-linux";
      configuration = config;
    };
  
in {
  system = (makeSystem configuration).system;
  vm = (makeSystem vmConfiguration).vm;
  tests = {
    prometheus = pkgs.callPackage ./tests/prometheus.nix { inherit configuration; };
    autoUpgrade = pkgs.callPackage ./tests/auto-upgrade.nix { };
    keys = pkgs.callPackage ./tests/keys.nix { };
    vpn = pkgs.callPackage ./tests/vpn.nix { };
    full = pkgs.callPackage ./tests/full.nix { inherit configuration; };
  };
}
