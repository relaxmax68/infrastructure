Ce dépot contient la configuration du serveur hébergeant les services
de `markas`.

Le serveur est joinable via `markas.fr`.


## Essayer le serveur localement

Il est possible de construire et démarrer une machine virtuelle
correspondant à la configuration du serveur. Cette machine peut être
utilisée pour tester un changement ou étudier la configuration du
serveur.

Il est nécessaire d'installer le gestionnaire de paquets
[Nix](https://nixos.org/nix/) pour construire cette machine
virtuelle. Pour installer Nix:

    curl https://nixos.org/nix/install | sh

Si besoin, le [manuel Nix](https://nixos.org/nix/manual/#chap-quick-start)
fournit plus d'information et d'options d'installation.

Une fois les outils Nix installés, il est possible de construire la
machine virtuelle depuis ce dépot Git:

Pour construire et démarrer cette machine:

    ./start-vm

Note: cette machine virtuelle utilise QEMU. Il est vivement conseillé
d'activer KVM afin de profiter de l'accéleration matérielle fournie
par le processeur. La documentation Ubuntu suivante fournies les
informations nécessaire à l'activation de KVM
<https://doc.ubuntu-fr.org/kvm>.

Une fois la machine virtuelle démarrée, il est possible de
s'authentifier avec le compte `root` et un mot de passe vide.

Les services suivant sont accessibles directement depuis la machien
hôte:

- graphana: <http://localhost:3000> (utiliser le username `admin` et
  le password `admin`)
- prometheus server: <http://localhost:9090>
- prometheus node exporter: <http://localhost:9100>
- ssh: `ssh -p 2222 root@localhost`

Note: La machine virtuelle ne démarre pas si les ports 3000, 9090,
9100 de la machine hôte sont utilisés.

## Comment tester un changement ou une Merge Request

Des [https://nixos.org/nixos/manual/index.html#sec-nixos-tests](tests
NixOS) sont disponibles pour valider certaines fonctionnalités du
serveur. Pour jouer ces tests:

    nix-build -A tests

La documentation
[https://nixos.org/nixos/manual/index.html#sec-nixos-tests](tests
NixOS) explique également comment jouer ces tests intéractivement.

## Comment soumettre une modification du serveur

Le serveur applique toute les minutes la configuration définie dans la
branche master de ce dépot. Pour appliquer une modification, il faut
donc soumettre une Merge Request (MR) à déstination de la branche
master. Une fois que cette MR est mergée, le serveur se met à jour une
minute plus tard.

Lorsque ce dépot est récupéré, la signature du commit `HEAD` est
vérifiée avec les clés publiques contenues dans le repertoire
`./keys`. La configuration est alors uniquement déployée si la
signature est valide. Cela permet de s'assurer qu'un attaquant ne
puisse pas compromettre le dépot de configuration.


## Comment upgrader NixOS

Le serveur est basé sur la dernière release NixOS. Pour suivre les
mise à jour, nous utilisons
[https://github.com/nmattia/niv](Niv). Pour suivre les mises à jour, il
faut donc faire:


    niv update nixpkgs

Il faut ensuite lancer les tests et commiter les fichiers modifiés.

Cette opération est actuellement manuelle mais pourra être effectuée
par la CI.


## Procédure d'installation de NixOS sur Kimsufi

Voir https://lewo.abesis.fr/posts/2019-12-01-install-nixos-on-kimsufi.html


## Signature des commits

Pour n'avoir que des commits signés et ne pas faire confiance à
Gitlab, les MR sont mergées en "fast forward".


## Secrets

Le module ../modules/keys.nix permet de définir des secrets utilisés
par d'autres modules. Chaque secret est spécifié dans un fichier. La
création de ce fichier est laissé à l'utilisateur (généralement créé
via ssh ou scp).

Lorsque qu'un secret est défini dans la configuration, un service
systemd est créé pour observer le fichier contenant ce secret. Ce
service systemd peut alors être utilisé comme dépendance d'un module
ayant besoin du secret contenu dans ce fichier.

Le test ../tests/keys.nix illustre cette fonctionnalité. Dans ce test,
un secret `foo` est utilisé par un service systemd `test`. Tant que le
fichier `/run/keys/foo` n'est présent, systemd bloque le démarrage du
service `test`. Une fois le fichier `/run/keys/foo` créé, le service
`test` est démarré.


## VPN

Un VPN est utilisé pour accéder certains des services de
l'infrastructure:

- journald: <vpn.markas.fr:19531>
- graphana: <vpn.markas.fr:3000>
- prometheus: <vpn.markas.fr:9090>

Ce VPN est basé sur Wireguard. La clé publique du VPN est
`jngKolQu7KZUw+vqKUjDRHF3C9PO8nnly69lPg8pbX0=`.

Un exemple de configuration cliente (pour NixOS) est disponible dans
le [test](./tests/vpn.nix). Cette configuration est à adapter en
fonction de votre distribution. IL faut également soumettre une
demande fusion avec votre clé Wireguard publique.

Note: le VPN rend votre machine utilisateur accessible depuis le
serveur. Il est donc préférable d'avoir un firewall!
