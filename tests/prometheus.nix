{ pkgs, configuration }:

with import (pkgs.path + /nixos/lib/testing.nix) { system = builtins.currentSystem; };

let
  jq = "${pkgs.jq}/bin/jq";
in
makeTest {
  name = "test-server";
  nodes = {
    machine = {pkgs, config, ...}: {
      imports = [ configuration ];

      config = {
        virtualisation = {
          diskSize = 1 * 1024;
        };
        # This service is used to validate a prometheus alert is fired
        # when a systemd service failed.
        systemd.services.unitFailed = {
          wantedBy = [ "multi-user.target" ];
          script = "false";
        };

        # It takes too much resource
        services.autoUpgrade.enable = pkgs.lib.mkForce false;

        # To output logs to check alertmanager is trying to send a mail
        services.prometheus.alertmanager.logLevel = "debug";
      };
    };
  };
  testScript = ''
    $machine->waitForUnit("prometheus.service");
    # Prometheus takes a bit of time to expose these job values
    $machine->waitUntilSucceeds("curl -L localhost:9090/api/v1/label/job/values | ${jq} -e '.data | sort | . == [\"node\", \"prometheus\"]'");

    $machine->waitUntilSucceeds("curl -s -L localhost:9090/api/v1/alerts | ${jq} -e '.data.alerts[] | select(.labels.name==\"unitFailed.service\") | .state == \"firing\"'");

    # Check if the alert manager gets the alert.
    # Note the alertmanager takes around 30sec to be notified.
    $machine->waitUntilSucceeds("curl localhost:9093/api/v2/alerts | ${jq} -e '.[] | select(.labels.name == \"unitFailed.service\")'");


    # TODO: test the test-weekly-alertmanager service

    # Check if the alertmanager is trying to send mails
    $machine->waitUntilSucceeds("journalctl -u alertmanager | grep 'msg=\"Notify attempt failed\"' | grep -q '10.100.0.4:25'")
  '';
}
