{ pkgs, git, wireguard }:

with import (pkgs.path + /nixos/lib/testing.nix) { system = builtins.currentSystem; };

let
  keyServer = pkgs.runCommand "key-server" { buildInputs = [ wireguard ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  keyMember = pkgs.runCommand "key-member" { buildInputs = [ wireguard ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  readPublicKey = filepath: builtins.replaceStrings ["\n"] [""] (builtins.readFile filepath);

  server = {pkgs, config, ...}: {
      imports = [ ../modules/vpn.nix ];
      config = {
        # TODO: this should not be part of this test but of the full test
        # instead.
        services.journald.enableHttpGateway = true;

        services.vpn.member = {
          clients = [{
            publicKey = readPublicKey "${keyMember}/public";
            ip = "10.100.0.2";
          }];
          privateKeyFile = "${keyServer}/private";
        };
      };
  };

  member = {pkgs, config, ...}: {
    config = {
      networking.wireguard.interfaces = {
        wg0 = {
          ips = [ "10.100.0.2/24" ];
          listenPort = 51820;
          privateKeyFile = "${keyMember}/private";
          peers = [
            {
              publicKey = readPublicKey "${keyServer}/public";
              allowedIPs = [ "10.100.0.1" ];
              endpoint = "server:51820";
              # This is to initialize the connection to the
              # server. Without this, the server can not communicate
              # to the client until the client sends a first request.
              persistentKeepalive = 25;
            }
          ];
        };
      };
    };
  };
in
makeTest {
  name = "test-vpn";
  nodes = { inherit server member; };
  testScript = ''
    $server->waitForUnit("multi-user.target");
    $member->waitForUnit("multi-user.target");
    $server->waitUntilSucceeds("ping -c 1 10.100.0.2");
  '';
}
