# This test checks the full configuration of the server
#
# Implemented tests are:
# - journalctl is reachable through the VPN

{ configuration, pkgs, git, wireguard }:

with import (pkgs.path + /nixos/lib/testing.nix) { system = builtins.currentSystem; };

let
  keyServer = pkgs.runCommand "key-server" { buildInputs = [ wireguard ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  keyMember = pkgs.runCommand "key-member" { buildInputs = [ wireguard ]; } ''
    mkdir $out
    wg genkey > $out/private
    wg pubkey < $out/private > $out/public
  '';

  readPublicKey = filepath: builtins.replaceStrings ["\n"] [""] (builtins.readFile filepath);

  server = {pkgs, config, ...}: {
    imports = [ configuration ];
    config = {
      services.vpn.member = {
        clients = [{
          publicKey = readPublicKey "${keyMember}/public";
          ip = "10.100.0.254";
        }];
        # We override the server public key for the test
        privateKeyFile = pkgs.lib.mkForce "${keyServer}/private";
      };
    };
  };
  
  member = {pkgs, config, ...}: {
    config = {
      networking.wireguard.interfaces = {
        wg0 = {
          ips = [ "10.100.0.254/24" ];
          listenPort = 51820;
          privateKeyFile = "${keyMember}/private";
          peers = [
            {
              publicKey = readPublicKey "${keyServer}/public";
              allowedIPs = [ "10.100.0.1" ];
              endpoint = "server:51820";
              # This is to initialize the connection to the
              # server. Without this, the server can not communicate
              # to the client until the client sends a first request.
              persistentKeepalive = 25;
            }
          ];
        };
      };
    };
  };
in
makeTest {
  name = "test-vpn";
  nodes = { inherit server member; };
  testScript = ''
    $server->waitForUnit("multi-user.target");
    $member->waitForUnit("multi-user.target");
    # Check if journald is exposed over the vpn
    $member->succeed("curl 10.100.0.1:19531");
    $member->fail("curl --max-time 4 server:19531");
    $member->succeed("curl 10.100.0.1:3000");
    $member->fail("curl --max-time 4 server:3000");
  '';
}
