## Journal

Ce journal contient toutes les actions manuelles réalisées sur l'infrastructure.

### 2020-01-06 - déploiement du VPN

- Il a fallu rebooter la machine pour que le module wireguard soit disponible
  ```
  Jan 06 19:28:22 nixos 1vc0ihxzjdqfq7fiwq76d2nypp0l95f0-unit-script-wireguard-wg-member-start[31603]: modprobe: FATAL: Module wireguard not found in directory /run/booted-system/kern...
  ```

- Les clés Wireguard sont manuellemt créées sur le serveur
  ```
  root$ wg genkey > /var/keys/wireguard-member
  Warning: writing to world accessible file.
  Consider setting the umask to 077 and trying again.
  root$ chmod 600 /var/keys/wireguard-member
  root$ wg pubkey < /var/keys/wireguard-member
  jngKolQu7KZUw+vqKUjDRHF3C9PO8nnly69lPg8pbX0=
  ```
